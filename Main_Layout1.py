# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 10:00:43 2021

@author: Brianna
"""

import sys

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QFileDialog, QMessageBox
from PyQt5.QtCore import QDir
from Layout1 import Layout1_GUI as ui
import time
import datetime



class app_window(QtWidgets.QMainWindow, ui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(app_window, self).__init__(parent)
        self.ui_tab = ui.Ui_MainWindow
        self.setupUi(self)
        
        self.button_add_file.clicked.connect(lambda: self.add_FileDialog_general('Select Report', 'Excel Files (*.xlsx *.xls *csv);;Python Files (*.py)', self.textBrowser_inFilePath))
        self.button_add_output.clicked.connect(lambda: self.add_FolderDialog_general('Select a Folder', self.textBrowser_outputDir))
        self.button_run.clicked.connect(self.run_script)

    def add_FileDialog_general(self, msg, file_types, update_browser):
        options = QFileDialog.Options()
        fileName, _ = QFileDialog.getOpenFileName(self, msg, "",file_types, options=options)
        if fileName:
            update_browser.setPlainText(fileName)
        
    def add_FolderDialog_general(self, msg, update_Browser):
         fname = QFileDialog.getExistingDirectory(self, 'Select a Directory')
        
         if fname:
            fname = QDir.toNativeSeparators(fname)
            update_Browser.setPlainText(fname)
            
         
    def run_script(self):
        # Script that running in background
        # Does stuff...
        
        self.message_box()
        time.sleep(10)
        
        ct = datetime.datetime.now()
        update = "Last Updated: {}".format(str(ct))
        self.label_result.setText(update)
        print('completed')
        
    def message_box(self):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("""Process running...\nUI will become unresponsive after clicking OK. To avoid unresponsive app, implement threading.""")
            
        msg.setWindowTitle("Message Box - Process Running")
        msg.setStandardButtons(QMessageBox.Ok)
        msg.exec()
        

def main():

    app = QApplication(sys.argv)
    form = app_window()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()