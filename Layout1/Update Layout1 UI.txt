Instructions to Update GUI for Layout1
-------------------------------------

1) Open an anacounda prompt in this directory

2) Type the following commands:
	pyuic5 Layout1.ui -o Layout1_GUI.py
	pyrcc5 resources.qrc -o resources_rc.py

3) Change the last line in "Layout1GUI.py"
   Original: import resources_rc
   Updated: from Layout1 import resources_rc