# General Logistics

## Setup
* For easiest use, need to have Anaconda installed

## Layouts
Layout 1 - Simple PyQt layout with dashboard framework, user add in file & out directory, results box
![Layout1-screenshot](/static/screenshots/Layout1.png)
